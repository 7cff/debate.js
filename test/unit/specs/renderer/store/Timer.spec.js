import Timer from '@/store/modules/Timer'

import sinon from 'sinon'

const { actions, mutations } = Timer

const state = {
  times: {
    extemp: 1000 * 60 * 30
  }
}

// These actions all depend on the same set of timer states, so ORDER MATTERS HERE.
describe('Timer store mutations', () => {
  // endTimer is really simple and shouldn't have any issues. Plus, trying to mock
  // that function would be pure hell.

  // getTime can't really be tested either due to its dependency on the new Date
  // object, of which we have no way of storing it. It's relatively simple anyways,
  // so we'll just depend on the fact that the math for the Timer is correct.
  // it('properly calculates the current time')

  it('properly starts the timer from a fresh state', () => {
    // Using stubbed functions since the other methods have already been tested.
    mutations.start(state, {
      interval: function () {},
      timer: function () {}
    })

    expect(state.initial).to.equal(state.times.extemp)
    expect(state.interval).to.be.a('number')
    expect(state.timeout).to.be.a('number')
    expect(state.startTime).to.be.a('number')
    expect(state.state).to.equal(1)
  })

  // Can't test clearTimeout or clearInterval, it's a JavaScript internal.
  it('properly pauses the timer', () => {
    mutations.pause(state)
    // eslint-disable-next-line
    expect(state.interval).to.be.null
    // eslint-disable-next-line
    expect(state.timeout).to.be.null
    expect(state.state).to.equal(2)
  })

  it('properly restarts a paused timer', () => {
    state.current = 1000 * 60 * 29

    mutations.start(state, {
      interval: function () {},
      timer: function () {}
    })

    // We already know the rest of the function works, but we need the initial time
    // to be correct.
    expect(state.initial).to.equal(1000 * 60 * 29)
  })

  it('properly stops and clears the timer', () => {
    mutations.stop(state)
    // eslint-disable-next-line
    expect(state.current).to.be.null
    // eslint-disable-next-line
    expect(state.interval).to.be.null
    // eslint-disable-next-line
    expect(state.timeout).to.be.null
    expect(state.state).to.equal(0)
  })

  it('properly times out the timer', (done) => {
    state.current = 1000

    // We know what the timer calls when done, but we need to confirm that done is
    // called in the first place.
    mutations.start(state, {
      interval: function () {},
      timer: done
    })
  })
})

describe('Timer store actions', () => {
  it('properly toggles the timer based on the current state', () => {
    const commit = sinon.spy()
    const state = { state: 0 }

    // We already know the arguments passed to the toggle function, but we want to
    // check the proper function is being called properly.
    actions.toggle({ commit, state })
    state.state = 1
    actions.toggle({ commit, state })
    state.state = 2
    actions.toggle({ commit, state })

    // We're not using deep equal in order to avoid the need to pass stub functions
    // due to the commit arguments on the actual timer.
    expect(commit.args[0][0]).to.equal('start')
    expect(commit.args[1][0]).to.equal('pause')
    expect(commit.args[2][0]).to.equal('start')
  })
})
