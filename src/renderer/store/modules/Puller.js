import ArticleGenerator from '@/../main/tasks/ArticleGenerator.js'
import router from '@/router/index'

const state = {
  articles: {}
}

const getters = {
  active: state => {
    // Technically this block of code will fail on other types of generators. However,
    // if it's another type of generator, there's probably already something wrong.
    return state.articles instanceof ArticleGenerator
  },
  puller: function (state) {
    return state.articles
  }
}

const mutations = {
  set (state, puller) {
    state.articles = puller
    console.log('Puller state updated.')
  }
}

const actions = {
  cancel (context) {
    context.commit('set', {})
    router.push({ name: 'index' })
  },
  next (context) {
    let puller = context.getters.puller
    let next = puller.next()
    if (next.done) {
      context.commit('set', {})
      $.notify({
        type: 'success',
        message: 'Pulling class parsed all articles.',
        progress: 100
      })
      router.push({ name: 'index' })
    } else {
      router.push({ name: 'articles.create', query: { link: next.value } })
      context.commit('set', puller)
    }
  },
  set (context, puller) {
    let article = puller.next().value
    console.log('Retrieved first article in puller.')
    router.push({ name: 'articles.create', query: { link: article } })
    console.log('Pushed articles.create route.')
    context.commit('set', puller)
    console.log('Committed updated puller instance.')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
