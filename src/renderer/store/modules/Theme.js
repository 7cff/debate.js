import cosmo from 'bootswatch/cosmo/bootstrap.min.css'
import darkly from 'bootswatch/darkly/bootstrap.min.css'
// TODO: Replace bootstrap-tokenfield.css with minified custom version that uses
// !important to override Bootstrap's default height setting.
import 'bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css'
import 'bootstrap-tokenfield/dist/css/tokenfield-typeahead.min.css'
cosmo.use()

const state = {
  // 0 = light, 1 = dark
  theme: 0
}

const getters = {
  theme: state => {
    return state.theme
  }
}

const mutations = {
  dark (state) {
    console.log('Setting theme to Darkly.')
    state.theme = 1
    cosmo.unuse()
    darkly.use()
  },
  light (state) {
    console.log('Setting theme to Cosmo.')
    state.theme = 0
    darkly.unuse()
    cosmo.use()
  }
}

const actions = {
  theme (context) {
    console.debug('Theme action caught.')
    context.state.theme ? context.commit('light') : context.commit('dark')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
