import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'landing-page',
    //   component: require('@/components/LandingPage').default
    // },
    {
      path: '/',
      name: 'index',
      component: require('@/components/Index').default
    },
    {
      path: '/articles',
      name: 'articles.index',
      component: require('@/components/article/Index').default
    },
    {
      path: '/articles/create',
      name: 'articles.create',
      component: require('@/components/article/Create').default
    },
    {
      path: '/articles/:id',
      name: 'articles.show',
      component: require('@/components/article/Show').default
    },
    {
      path: '/articles/:id/edit',
      name: 'articles.edit',
      component: require('@/components/article/Edit').default
    },
    {
      path: '/pull',
      name: 'pull',
      component: require('@/components/Pull').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
