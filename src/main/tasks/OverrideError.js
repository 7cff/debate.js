class OverrideError extends Error {
  constructor (message) {
    super(message)
    this.name = 'OverrideError'
    this.message = 'This method must be overriden.'
  }
}
export default OverrideError
