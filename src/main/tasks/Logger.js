import winston from 'winston'

const logger = new winston.Logger({
  level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
  transports: [
    new (winston.transports.Console)()
  ]
})

export default logger
