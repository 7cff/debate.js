export default function* ArticleGenerator (reader, articles, that) {
  for (let article of articles) {
    that.emit('progress', {
      loaded: articles.indexOf(article) + 1,
      total: articles.length
    })
    yield new that.readers[reader](article)
  }
  that.removeAllListeners()
}
