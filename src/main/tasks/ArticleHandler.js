import ArticleGenerator from './ArticleGenerator'
import BaseReader from './BaseReader'
import { app } from 'electron'
import EventEmitter from 'events'
import { mkdir, readdir } from 'fs'
import loadjs from './loadjs'
// import fs from 'mz/fs';
import { resolve } from 'path'
import validUrl from 'valid-url'
import logger from './Logger'

/**
 * This class wraps around the various article readers in order to determine the
 * appropriate reader to use and to run it.
 * @access private
 * @class
 */
class ArticleHandler extends EventEmitter {
  constructor () {
    super()
    logger.info('Instantiating ArticleHandler.')
    this.pullers = []
    this.readers = []
  }

  /**
   * Parses the URL for the appropriate reader, then loads the reader and parses
   * the article if one is found.
   *
   * If the URI is improperly formatted, an Error will be thrown.
   *
   * @param {string} url
   *
   * @returns {Article} An Article holding the data of the parsed link.
   *
   * @throws {URIError} Will throw an error if the URI is malformed.
   */
  download (url) {
    if (!validUrl.isUri(url)) {
      throw new URIError('URI malformed')
    }
    let basename = url.split('//')[1].split('/')[0]
    for (let Reader of this.readers) {
      if (Reader.basename.localeCompare(basename) === 0) {
        // eslint-disable-next-line no-console
        logger.debug('Reader found. Parsing.')
        return new Reader(url)
      }
    }
    logger.warn('Could not find a reader for this article.')
    throw new Error('Could not find reader!')
  }

  nullify () {}

  /**
   * Given the id number of a reader, the function will attempt to call that reader's
   * pull function and handle the returned array of urls. These urls will then
   * be parsed accordingly to download the articles that were fetched.
   *
   * @param {number} reader The id of the reader. This refers to its location in
   * the array containing all the readers.
   *
   * @returns {}
   */
  async pull (reader) {
    let articles = await new this.readers[reader]().pull()
    let that = this
    return ArticleGenerator(reader, articles, that)
  }

  /**
   * Loads all the readers from the configuration directory.
   *
   * Note that this function will dump ALL existing reader instances and reload
   * them from scratch.
   *
   * Dev note: I wanted to clean this up with Promises/async but good old
   * Webpack decided to ruin my day:
   * https://github.com/thenables/thenify/blob/master/index.js
   * ReferenceError: createCallback is not defined
   */
  reload () {
    logger.debug('Dumping any existing readers.')
    this.readers = []
    readdir(resolve(app.getPath('userData'), 'readers'), (err, readerPaths) => {
      if (err) {
        if (err.code === 'ENOENT') {
          mkdir(resolve(app.getPath('userData'), 'readers'), err => {
            if (!err) {
              logger.warn('The readers folder did not exist and was created. You should only see this message on first run.')
            }
          })
        }
      } else {
        for (let path of readerPaths) {
          logger.debug('Loading ' + path + '.')
          try {
            let builder = loadjs(resolve(app.getPath('userData'), 'readers', path))
            let reader = builder(BaseReader)
            if (this.testParser(reader)) {
              this.readers.push(reader)
              logger.debug('Loaded ' + path + ' as reader. Now trying to load puller.')
              if (this.testPuller(reader)) {
                this.pullers.push(reader)
                logger.debug('Loaded ' + path + ' as puller.')
              } else {
                logger.warn('The puller at ' + path + ' did not load. One or more functions was not overriden.')
              }
            } else {
              logger.warn('The reader at ' + path + ' did not load. One or more functions was not overriden.')
            }
          } catch (e) {
            logger.warn('Error occured when loading ' + path + ':')
            logger.warn(e)
          }
        }
        logger.info('Loaded ' + this.readers.length + ' reader(s).')
      }
    })
  }

  testParser (Reader) {
    let props = ['basename', 'source']
    let download = ['_content', '_title']
    let errors = []
    let instance = new Reader()

    for (let func of props) {
      try {
        this.nullify(Reader[func])
      } catch (e) {
        errors.push(e)
      }
    }
    for (let func of download) {
      try {
        instance[func]()
      } catch (e) {
        errors.push(e)
      }
    }

    for (let error of errors) {
      // Important note: error.constructor DOES NOT WORK. Screw JS.
      if (error.name === 'OverrideError') {
        return false
      }
    }
    return true
  }

  testPuller (Reader) {
    let errors = []

    try {
      this.nullify(Reader._homepage)
    } catch (e) {
      errors.push(e)
    }

    try {
      Reader._articles()
    } catch (e) {
      errors.push(e)
    }

    for (let error of errors) {
      if (error.name === 'OverrideError') {
        return false
      }
    }
    return true
  }

  // reload() {
  //   this.readers = [];
  //   // console.debug('Getting contents of readers directory.');
  //   fs.readdir(path.resolve(app.getPath('userData'), 'readers')).then(readerPaths => {
  //     for (let reader of readerPaths) {
  //       // console.debug('Loading ' + reader + '.');
  //       this.readers.push(loadjs(path.resolve(app.getPath('userData') + '/readers', reader))(BaseReader));
  //     }
  //     // eslint-disable-next-line no-console
  //     console.log('Loaded ' + readerPaths.length + ' reader(s).')
  //   }).catch(function(error) {
  //     if (error.code === 'ENOENT') {
  //       fs.mkdir(path.resolve(app.getPath('userData'), 'readers')).then(function() {
  //         console.warn('The readers folder did not exist and was created. You should only see this message on first run.');
  //       });
  //     }
  //     console.log(error)
  //   });
  // }
}

const instance = global.handler = new ArticleHandler()

export default instance
